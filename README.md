# Sales Navigator Export Converter

https://boy51.gitlab.io/salesnavigator-export-converter/

Easily convert export from sales navigator to CSV. This tool runs completely offline and doesn't steal your lead data.

## How to use

1. Go to Sales navigator
2. Open dev tools network tab
3. Go to a lead search or lead list with network tab open
4. For lead search, the request is `salesApiLeadSearch`. For lead list, the request is `salesApiPeopleSearch`. We want to copy `elements` which is a JSON array of results. Can be copied in chrome via preview - right click `elements` - copy value.
5. Paste into the tool. Click "+" to add records. Repeat step 1 - 4 for all pages of your search/list.
6. Once all your records have been added, click the CSV button.
