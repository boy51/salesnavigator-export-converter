import { Heading, Text, VStack } from "@chakra-ui/layout";
import { useState } from "react";
import "./App.css";
import { FieldsMapper } from "./components/fields-mapper.component";
import { useColStore } from "./components/fields-mapper.store";
import { GenerateCsvButton } from "./components/generate-csv-button.component";
import { JsonInput } from "./components/json-input.component";
import { JSONPreview } from "./components/json-preview.component";
import { parseColumns } from "./functions/parse-mapped-field";
import { PersonSearchResult } from "./typings/person";

function App() {
  const [payload, setPayload] = useState<PersonSearchResult[]>([]);
  const store = useColStore();

  return (
    <VStack>
      <Heading mb={6}>Sales navigator export</Heading>
      <JsonInput
        onAdd={result => {
          if (result) setPayload(p => [...p, ...result]);
        }}
        onClear={() => setPayload([])}
      />
      <Text>{payload.length} records added</Text>
      {!payload.length && <Text>Add JSON to continue...</Text>}
      {payload.length && <GenerateCsvButton payload={payload} />}
      {payload.length && <JSONPreview data={payload[0]} />}
      {payload.length && <FieldsMapper />}
      {payload.length && (
        <JSONPreview
          buttonText="parsed sample"
          data={parseColumns(store.value, payload[0])}
        />
      )}
    </VStack>
  );
}

export default App;
