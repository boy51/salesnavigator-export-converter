export interface CurrentPosition {
  current: boolean;
  companyName: string;
  title: string;
  companyUrn: string;
}

export interface Artifact {
  width: number;
  fileIdentifyingUrlPathSegment: string;
  height: number;
}

export interface ProfilePictureDisplayImage {
  artifacts: Artifact[];
  rootUrl: string;
}

export interface PersonSearchResult {
  lastName: string;
  memorialized: boolean;
  geoRegion: string;
  saved: boolean;
  $anti_abuse_metadata: unknown;
  uniquePositionCompanyCount: number;
  currentPositions: CurrentPosition[];
  entityUrn: string;
  profilePictureDisplayImage: ProfilePictureDisplayImage;
  blockThirdPartyDataSharing: boolean;
  pendingInvitation: boolean;
  degree: number;
  fullName: string;
  listCount: number;
  firstName: string;
  dateAddedToListAt: number;
}
