/**
 * @see https://support.pipedrive.com/en/article/importing-data-into-pipedrive-with-spreadsheets
 */
export interface CsvResult {
  personName: string;
  organizationName: string;

  /** Make lead or deal? */
  dealTitle?: string;
  leadTitle?: string;
}
