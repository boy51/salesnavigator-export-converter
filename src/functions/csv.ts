const _replacer = (key: string, value: string) => (!value ? "" : value);

/**
 * Converts a JSON object to a CSV string.
 *
 * @param {T[]} json - The JSON object to convert to a CSV string. The objects in the array must have string keys.
 * @returns {string} - The resulting CSV string.
 */
export function jsonToCsv<T extends { [key: string]: string }>(json: T[]): string {
  const fields = Object.keys(json[0]);
  const csv = json.map(row =>
    fields.map(fieldName => JSON.stringify(row[fieldName], _replacer)).join(",")
  );
  csv.unshift(fields.join(","));
  return csv.join("\r\n");
}
