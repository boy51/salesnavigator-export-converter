import { ColumnConfig } from "../components/fields-mapper.store";

export function parseColumns(columns: ColumnConfig[], data: any): any {
  const mm = columns.map(col => {
    let val = data[col.accessor];
    const key = col.title || col.accessor;

    if (col.deepAccessor?.length) {
      for (const deepacc of col.deepAccessor) {
        try {
          val = val[deepacc];
        } catch (err) {
          val = "Invalid accessor: " + deepacc;
          break;
        }
      }
    }

    return { [key]: val };
  });

  return mm.reduce((acc, val) => {
    acc = { ...acc, ...val };
    return acc;
  }, {});
}

export function mapParsed(columns: ColumnConfig[]) {
  return (data: any) => parseColumns(columns, data);
}
