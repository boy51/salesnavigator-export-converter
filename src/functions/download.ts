export function downloadStringAsFile(string: string, fileName: string) {
  const a = document.createElement("a");
  a.href = URL.createObjectURL(new Blob([string], { type: "text/csv" }));
  a.download = fileName;
  a.click();
}
