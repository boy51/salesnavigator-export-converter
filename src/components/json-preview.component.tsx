import { Button } from "@chakra-ui/button";
import { Textarea } from "@chakra-ui/textarea";
import { useState } from "react";

export const JSONPreview: React.FC<{ data: any; buttonText?: string }> = ({
  data,
  buttonText = "data sample",
}) => {
  const [show, setShow] = useState(false);
  return (
    <>
      <Button onClick={() => setShow(p => !p)}>
        {!show ? "Show" : "Hide"} {buttonText}
      </Button>
      {show && <Textarea height={500}>{JSON.stringify(data, null, 2)}</Textarea>}
    </>
  );
};
