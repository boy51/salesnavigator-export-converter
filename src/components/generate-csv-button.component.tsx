import { Button } from "@chakra-ui/button";
import { Input } from "@chakra-ui/input";
import { HStack } from "@chakra-ui/layout";
import { DateTime } from "luxon";
import { useCallback, useState } from "react";
import { jsonToCsv } from "../functions/csv";
import { downloadStringAsFile } from "../functions/download";
import { mapParsed } from "../functions/parse-mapped-field";
import { useColStore } from "./fields-mapper.store";

export const GenerateCsvButton: React.FC<{ payload: any }> = ({ payload }) => {
  const store = useColStore();
  const [fileName, setFileName] = useState(
    "sn-export-" + DateTime.now().toFormat("yyyy-MM-dd")
  );
  const generateCsv = useCallback(() => {
    const parsed = jsonToCsv(payload.map(mapParsed(store.value)) as any);

    downloadStringAsFile(parsed, fileName + ".csv");
  }, [payload, fileName]);

  if (!payload?.length) return null;
  return (
    <HStack>
      <Input
        value={fileName}
        onChange={event => setFileName(event.currentTarget.value)}
      />
      <Button colorScheme="green" onClick={generateCsv} width={200}>
        Give me my CSV
      </Button>
    </HStack>
  );
};
