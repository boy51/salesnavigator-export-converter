import { HStack } from "@chakra-ui/layout";
import { Button, Textarea } from "@chakra-ui/react";
import { useCallback, useState } from "react";
import { PersonSearchResult } from "../typings/person";

type P = {
  onAdd?: (value: PersonSearchResult[] | undefined, error?: string) => void;
  onClear?: () => void;
};

export const JsonInput: React.FC<P> = ({ onAdd, onClear }) => {
  const [value, setValue] = useState("");
  const [error, setError] = useState<string>();
  const parseValue = useCallback<() => undefined | PersonSearchResult[]>(() => {
    if (!value) return;

    return JSON.parse(value);
  }, [value]);

  return (
    <>
      <HStack mb={2}>
        <Textarea
          placeholder="Enter JSON..."
          value={value}
          onChange={e => {
            setValue(e.currentTarget.value);
          }}
        />{" "}
        <Button
          onClick={() => {
            let result = parseValue();
            if (!result?.length) {
              result = [];
              setError("Invalid input, nothing was added");
            }

            onAdd && onAdd(result);
            setValue("");
          }}
          colorScheme={Array.isArray(parseValue()) ? "green" : "gray"}
        >
          +
        </Button>
        <Button onClick={onClear}>Clear</Button>
      </HStack>
      <span>{error}</span>
    </>
  );
};
