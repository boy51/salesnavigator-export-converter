import { Button } from "@chakra-ui/button";
import { Input, InputGroup, InputLeftAddon } from "@chakra-ui/input";
import { Box, Divider, Heading, HStack, SimpleGrid } from "@chakra-ui/layout";
import { useState } from "react";
import { ColumnConfig, useColStore } from "./fields-mapper.store";

export const FieldsMapper: React.FC = () => {
  const [show, setShow] = useState(false);
  const store = useColStore();

  return (
    <>
      <Button onClick={() => setShow(p => !p)}>
        {!show ? "Show" : "Hide"} field settings
      </Button>
      {show && <FieldSettingsList config={store.value} />}
    </>
  );
};
const FieldSettingsList: React.FC<{ config: ColumnConfig[] }> = ({ config }) => {
  const store = useColStore();

  return (
    <Box>
      {config.map((cfg, i) => (
        <>
          <FieldSettings
            name={`Column ${i + 1}`}
            {...cfg}
            onAdd={store.add}
            onRemove={i !== 0 ? () => store.remove(i) : undefined}
            onChange={val => store.set(i, val)}
          />
          <Divider m={4} />
        </>
      ))}
    </Box>
  );
};

type ColProps = {
  onAdd?: () => void;
  onRemove?: () => void;
  onChange?: (val: ColumnConfig) => void;
};

const FieldSettings: React.FC<
  {
    name?: string;
  } & ColumnConfig &
    ColProps
> = ({
  name = "Column",
  title,
  accessor,
  deepAccessor = [],
  onAdd,
  onRemove,
  onChange,
}) => {
  return (
    <>
      <HStack justify="flex-start" mb={2}>
        <Heading alignContent="flex-start" size="md">
          {name}
        </Heading>
        <Button onClick={onAdd}>+</Button>
        <Button onClick={() => onRemove && onRemove()} disabled={!onRemove}>
          -
        </Button>
      </HStack>
      <SimpleGrid columns={2} spacing={1}>
        <Box>
          <InputGroup>
            <InputLeftAddon children="Column title" />
            <Input
              placeholder="Fall back to accessor"
              value={title}
              onChange={event => {
                onChange && onChange({ title: event.currentTarget.value, accessor });
              }}
            />
          </InputGroup>
        </Box>
        <Box>
          <InputGroup>
            <InputLeftAddon children="Accessor" />
            <Input
              placeholder="lastName"
              value={accessor}
              onChange={event => {
                onChange && onChange({ title, accessor: event.currentTarget.value });
              }}
            />
          </InputGroup>
        </Box>
      </SimpleGrid>
      <Box>
        <Box>
          <Button
            variant="ghost"
            onClick={() =>
              onChange &&
              onChange({ title, accessor, deepAccessor: [...deepAccessor, ""] })
            }
          >
            Add Nested accessor...
          </Button>
        </Box>
        <DeepAccessorSettings
          title={title}
          accessor={accessor}
          deepAccessor={deepAccessor}
          onChange={onChange}
        />
      </Box>
    </>
  );
};

const DeepAccessorSettings: React.FC<ColumnConfig & Pick<ColProps, "onChange">> = ({
  title,
  accessor,
  deepAccessor,
  onChange,
}) => {
  return (
    <>
      {deepAccessor?.length
        ? deepAccessor.map((deepAccessor, i) => (
            <HStack justify="flex-start">
              <InputGroup mb={2}>
                <InputLeftAddon children={`Accessor ${i + 1}`} width={130} />
                <Input
                  value={deepAccessor}
                  onChange={event => {
                    onChange &&
                      onChange({
                        title,
                        accessor: accessor,
                        deepAccessor: [
                          ...deepAccessor.slice(0, i),
                          event.currentTarget.value,
                          ...deepAccessor.slice(i + 1),
                        ],
                      });
                  }}
                />
              </InputGroup>
              <Button
                onClick={() => {
                  onChange &&
                    onChange({
                      title,
                      accessor,
                      deepAccessor: [
                        ...deepAccessor.slice(0, i - 1),
                        ...deepAccessor.slice(i),
                      ],
                    });
                }}
              >
                -
              </Button>
            </HStack>
          ))
        : undefined}
    </>
  );
};
