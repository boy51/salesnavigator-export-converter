import { create } from "zustand";

export type ColumnConfig = {
  /** Same as accessor if not provided */ title?: string;
  accessor: string;
  deepAccessor?: string[];
};

type ColumnsConfigStore = {
  value: ColumnConfig[];
  add: () => void;
  remove: (index: number) => void;
  set: (index: number, val: ColumnConfig) => void;
};

const defaultColumnsCfg: ColumnConfig[] = [
  { accessor: "firstName" },
  { accessor: "lastName" },
  { accessor: "geoRegion" },
  {
    title: "companyName",
    accessor: "currentPositions",
    deepAccessor: ["0", "companyName"],
  },
  { title: "position", accessor: "currentPositions", deepAccessor: ["0", "title"] },
];

export const useColStore = create<ColumnsConfigStore>(set => ({
  value: defaultColumnsCfg,
  add: () => set(state => ({ value: [...state.value, { accessor: "" }] })),
  remove: i =>
    set(state => ({
      value: [...state.value.slice(0, i - 1), ...state.value.slice(i)],
    })),
  set: (i, val) =>
    set(state => ({
      value: [
        ...state.value.slice(0, i),
        { ...state.value[i], ...val },
        ...state.value.slice(i + 1),
      ],
    })),
}));
